<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(ProductsSeeder::class);
        DB::table('products')->insert([
            'name'=>'God of War',
            'price'=>12.5,
            'stock'=>10,
            'description'=>'descripcion product',
            'image'=>base64_encode(file_get_contents('/home/itb/tienda/app/img/gow.jpg')),

    ]);

        DB::table('products')->insert([
            'name'=>'Red Dead Redemption 2',
            'price'=>12.5,
            'stock'=>10,
            'description'=>'descripcion product',
            'image'=>base64_encode(file_get_contents('/home/itb/tienda/app/img/rdr2.jpeg')),

        ]);


        DB::table('products')->insert([
            'name'=>'Persona 5',
            'price'=>12.5,
            'stock'=>10,
            'description'=>'descripcion product',
            'image'=>base64_encode(file_get_contents('/home/itb/tienda/app/img/persona5.jpeg')),

        ]);

    }
}
