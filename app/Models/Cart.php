<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $fillable=[
      "id",
    ];
    public $timestamps = false;

    public function user(){
        $this->belongsTo("App\Models\User");
    }
}
