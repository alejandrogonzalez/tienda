<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Cart_Items;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Products;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function prueba(Request $request){
        $products = DB::table('products')->select('name','product name')->get();
        return view('products')->with('products',$products);
    }

    public function show(Request $request){
        $valueMin = empty($request->input('priceMin'))?'0':$request->input('priceMin') ;
        $valueMax = empty($request->input('priceMax'))?'100000':$request->input('priceMax') ;

        $all = DB::select(DB::raw("select * from products where price BETWEEN $valueMin AND $valueMax"));

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 4;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products',$products);
    }
    public function AddToCart($id){
        $product = Products::find($id);

        Log::channel('buy')->info('ejemplo'.$id);



        Log::error('error');
        Log::warning('warning');
        Log::notice('notice');
        Log::info('into');
        Log::debug('debug');

        Log::channel('buy')->info('Ejemplo log');
        Log::channel('single')->info('Ejemplo log');

        $user = Auth::user()->id;
        $cart_bd = Cart::where("user_id",$user)->first();

        if (empty($cart)){
            $cart_bd = new Cart();
            $cart_bd -> user_id = $user;
            $cart_bd -> save();
        }

        if(!$product){
            abort(404);
        }
        $cart = session() -> get('cart');
        if(!$cart){
            $cart = [
                $id => [
                    'name' => $product -> name,
                    'amount' =>  1,
                    'price' => $product -> price,
                    'image' => $product -> image,
                ]
            ];

            $cart_items = new Cart_Items();
            $cart_items -> cart_id = $cart_bd->id;
            $cart_items -> product_id = $id;
            $cart_items -> amount = 1;
            $cart_items -> save();


            session()->put('cart',$cart);
            return redirect()->back()->with('success','Producto añadido correctamente');
        }
        if(isset($cart[$id])){
            $cart[$id]['amount']++;
            $cart_items = Cart_Items::where(['cart_id', $cart->id],['product_id',$id])->first();
            $cart_items -> amount = $cart[$id]['amount'];
            $cart_items -> update();
            session()->put('cart',$cart);
            return redirect()->back()->with('success','Producto añadido correctamente');
        }

        $cart[$id] =[
            'name' => $product -> name,
            'amount' => 1,
            'price' => $product -> price,
            'image' => $product -> image,
            ];

        session()->put('cart',$cart);
        return redirect()->back()->with('success','Producto añadido correctamente');
    }

    public function update(Request $request){
        if($request->id && $request->amount){
            $cart = session()->get('cart');
            $cart[$request->id]['amount'] = $request->amount;
            $user = Auth::user()->id;
            $cart_bd = Cart::where('user_id',$user)->first();
            $cart_item = Cart_Items::where([['cart_id',$cart_bd->id],['product_id',$request->id]])->first();
            $cart_item->amount=$request->amount;
            $cart_item->update();
            session()->put('cart',$cart);
            session()->flash('success','Carro actualizado');
        }
    }
    public function remove(Request $request){
        if($request->id){
            $cart = session()->get('cart');
            if(isset($cart[$request -> id])){
                unset($cart[$request->id]);
                if($request->id && $request->amount) {
                    $cart = session()->get('cart');
                    $cart[$request->id]['amount'] = $request->amount;
                    $user = Auth::user()->id;
                    $cart_bd = Cart::where('user_id', $user)->first();
                    $cart_item = Cart_Items::where([['cart_id',$cart_bd->id],['product_id',$request->id]])->first();
                    $cart_item->delete();

                    session()->put('cart', $cart);
                }
            }
            session()->flash('success','Carro actualizado');
        }
    }

    public function cart(){
        return view('cart');
    }
}
