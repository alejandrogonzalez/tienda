@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Products') }}</div>
                <div class="card-body">
                    @foreach ($products as $product)

                        <li>{{$product->name}}</li>
                        <li>{{$product->price}}</li>

                        <img src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"/>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
