@extends('layouts.app')

@section('content')

<style>

    .product{

        height: 200px;
        width: 200px;
        border: 1px solid grey;
        margin: 20px;
        background: lightblue;
    }

    .card-body{
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    }

    img{
        margin-top:10px;
        width: 170px;
        margin-left: 15px;

    }

    .btn-holder{
        color: red;
        height: 10px;
        width:10px;
    }
</style>

    <div class="container">
        <div class="card-header">
            <form method="get" action="/products">
                Filtro de precios
                <input name="priceMin" type="text" placeholder="Precio Mínimo"/> -
                <input name="priceMax" type="text" placeholder="Precio Máximo"/>
                <input name="buscar" type="submit"/>
            </form>
            <div class="btn-carrito"><a href="{{url('cart/')}}">Ir al Carrito</a></div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Register') }}</div>
                    <div class="card-body">
                        @foreach ($products as $product)
                            <div class="product">
                                {{$product->name}}
                                {{$product->price}}
                                <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">añadir</a> </p>

                                <img src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"/>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{$products->links()}}
    </div>
@endsection
